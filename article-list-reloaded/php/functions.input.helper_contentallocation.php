<?php
/***********************************************************
*  Various helping functions for the Input area of modules *
*  Only Content Allocation Functions                       *
*  Author:  B. Behrens, HerrB                              *
*  Version: 1.3                                            *
************************************************************/


defined('CON_FRAMEWORK') || die('Illegal call: Missing framework initialization - request aborted.');


    function listOptionsAllocation($parent = 0, $language, $level = 0, $sSelected) {
        global $cfg, $client, $lang, $idcat;
       
       $html = "";
       $db = cRegistry::getDb();
       $sql = "";
       $sql = "SELECT a.idpica_alloc as idpica_alloc, a.name as name FROM ".$cfg["tab"]["pica_lang"]." AS a JOIN ".$cfg["tab"]["pica_alloc"]." AS b on a.idpica_alloc=b.idpica_alloc WHERE b.parentid = $parent AND a.idlang=$lang ORDER BY b.sortorder asc";
       $db->query($sql);
       if ($db->numRows() == 0) {
          return "";
       }
       $sArray = explode(",",$sSelected);   
       while ($db->nextRecord()) {
          $html .= '<option value="'.$db->f("idpica_alloc").'"';
          if (in_array($db->f("idpica_alloc"),$sArray))
             $html .= ' selected="selected"';
          $html .= '>'."\n";
          for ($i=0;$i<$level;$i++) {
             $html .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
          }
          $html .= /*">".*/$db->f("name")."</option>";
          $html .= listOptionsAllocation($db->f("idpica_alloc"),$language,$level+1, $sSelected);
       }
       unset ($db2);
        unset ($sql);

       return $html;

    }

    function buildAllocationSelect($varName, $varValue, $sType = '', $sSize = '1') {
       $html = "";
       $html .= '<SELECT name="'.$varName.'" '.$sType.' size='.$sSize.'>'."\n";
       $html .= '  <option value="">'.i18n("Bitte w&auml;hlen (ersetzt die oben stehende Auswahl)").'</option>'."\n";
       $html .= listOptionsAllocation(0,$lang, 0, $varValue);
       $html .= "</SELECT>";
       return $html;
    }




?>
