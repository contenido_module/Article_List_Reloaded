?>

<style type="text/css">
	fieldset {margin-top: 15px; background-color: #F4F4F4; padding: 0;}
	fieldset table {width: 100%;}
	fieldset table td input.follow { margin-left: 5px;}
	fieldset table td input.follow2 { margin-left: 20px;}
	h4 { font-size: 13px; font-weight: bold;}
	.silverline {border-bottom: 1px solid silver; }
	.silverlinetop {border-top: 1px solid silver; }
	.darkgray {background-color: #e4e4e4}
	input, select {margin-right: 10px;}
	div.checkbox_wrapper { display: inline; }
	.morebutton {margin-right: 5px; padding: 0 5px; background: #0060B1; color: white; text-align: center; border: 1px solid black;}
	.morebutton2 {margin-right: 5px; padding: 0 5px; background: white; color: #0060B1; text-align: center;border: 1px solid black;}
	span.showall {display: none;}
</style>

<?php
// Includes
if ( !function_exists("getChildPicas") ) { cInclude("module", "js.input.alr.php"); }
if ( !function_exists("fncBuildCategorySelect") ) { cInclude("module", "functions.input.helper.gw.php"); }
cInclude("module", "functions.input.helper_contentallocation.php"); // Tagging

// Initialization
$bDebug          = false;
$iDataStart      = 1000; // Startwert fuer dynamisch generierte CMS_VAR Elemente !!!Muss im Output gleich sein!!!
$sSubmitLink     = '<a href="javascript: if (document.tplcfgform.send) {document.tplcfgform.send.value = 0}; document.tplcfgform.submit();"><img class="submitbutton" src="images/submit.gif" title="'.mi18n("Save changes").'" /></a>';

// Start der Output-Ausgabe
$db = cRegistry::getDb();
$cfg = cRegistry::getConfig();
$client = cRegistry::getClientId();
$cfgClient = cRegistry::getClientConfig();

$adddir_checked_none = ( "CMS_VALUE[105]" == "none" ) ? "checked" : ""; 
$adddir_checked_below = ( "CMS_VALUE[105]" == "below" ) ? "checked" : ""; 
$adddir_checked_select = ( "CMS_VALUE[105]" == "select" ) ? "checked" : ""; 

// find Smarty-Templates which belong to THIS module by scanning the appropriate subdir
$module = new cModuleHandler($cCurrentModule);
$tplFiles = $module->getAllFilesFromDirectory('template');
array_multisort($tplFiles, SORT_ASC, SORT_STRING);

// construct the HTML table
$filltable = array();
$fillrow   = array();
$table = new cHTMLTable();
$table->setWidth("100%");
//first row with putting module on-/offline
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_online"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[0]");
$select->autofill(array(""=>mi18n("please_choose"), "true"=>"aktiv", "false"=>"inaktiv"));
$select->setDefault("CMS_VALUE[0]");
$td->setContent($select.$sSubmitLink);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//start next row with choice of show/hide setting boxes
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_behaviour"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$checkbox = new cHTMLCheckbox("CMS_VAR[10]", "true");
$checkbox->setLabelText( mi18n("keep_menu_open") );
$checkbox->setChecked( "CMS_VALUE[10]" );
$td->setContent($checkbox);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// put all rows in the table and echo this first table
$table->setContent($filltable);
echo $table->render();

// construct the first table to include in a fieldset
$faqclass = ( "CMS_VALUE[10]" ) ? "showall" : "showmore";

$filltable = array();
$table = new cHTMLTable();
//first row with categories
$fillrow   = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_category_main"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = buildCategorySelect("CMS_VAR[100]", "CMS_VALUE[100]", "0");
$td->setContent($input);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//start next row with additional category choices
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_adddir"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLRadiobutton("CMS_VAR[105]", 'none', '', $adddir_checked_none);
$input->setLabelText(mi18n("value_none"));
$td->setContent($input);
$input2 = new cHTMLRadiobutton("CMS_VAR[105]", 'below', '', $adddir_checked_below);
$input2->setLabelText(mi18n("value_below"));
$td->setContent($input."  ".$input2);
$input3 = new cHTMLRadiobutton("CMS_VAR[105]", 'select', '', $adddir_checked_select);
$input3->setLabelText(mi18n("value_select"));
$td->setContent($input."  ".$input2."  ".$input3);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// construct the additional categories select
$fillrow   = array();
$table = new cHTMLTable();
//first row with categories
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent("");
$fillrow[] = $td;
$td = new cHTMLTableData();
//$select = new cHTMLSelectElement();
$select = fncBuildCategorySelect("adddir".$cCurrentContainer, "CMS_VALUE[106]" , 0, "fncUpdateSel( 'adddir".$cCurrentContainer."', '"."CMS_VAR[106]"."');", "10", "multiple", false, 0, false);
$input = new cHTMLHiddenField("CMS_VAR[106]", "CMS_VALUE[106]");
//$td->setContent(fncAddMultiSelJS().$select.$input); // geändert nach Umlagerung der Funktion in andere Datei
$td->setContent($select.$input);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;


// *** Content Allocation / Tagging Category - replaces other choices!

// check if tagging categories exist
$db6 = cRegistry::getDb();
$sql = "";
$sql = "SELECT a.idpica_alloc as idpica_alloc FROM ".$cfg["tab"]["pica_alloc"]." AS a WHERE a.parentid = 0";
$db6->query($sql);
if ($db6->numRows() > 0) {
	
	/* unkomfortable Variante mit Texteingabe - aber mehrere unabhängige Kategorien möglich! 
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("pica_startcategory"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[610]", "CMS_VALUE[610]",'21','20','pica_startcategory',false,null,'');
$td->setContent('<span title="Mehrere durch Komma trennen">Tagging-Kategorie:</span>'.$input);
//$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;
*/
	
	// *** Content Allocation / Tagging Category - replaces other choices!
	
	//first row with tagging categories
	$fillrow   = array();
	$tr = new cHTMLTableRow();
	$td = new cHTMLTableData();
	$td->setContent(mi18n("label_tagging_category_main"));
	$fillrow[] = $td;
	$td = new cHTMLTableData();
	$input = buildAllocationSelect("CMS_VAR[610]", "CMS_VALUE[610]", "0");
	$td->setContent($input);
	$fillrow[] = $td;
	$tr->setContent($fillrow);
	$filltable[] = $tr;
	
	if ( "CMS_VALUE[620]" == "none" ) $addtag_checked_none = "checked"; 
	if ( "CMS_VALUE[620]" == "below" ) $addtag_checked_below = "checked"; 
	
	//start next row with additional tagging category choices
	$fillrow = array();
	$tr = new cHTMLTableRow();
	$td = new cHTMLTableData();
	$td->setContent(mi18n("label_tagging_adddir"));
	$td->setClass("silverline");
	$fillrow[] = $td;
	$td = new cHTMLTableData();
	$input = new cHTMLRadiobutton("CMS_VAR[620]", 'none', '', $addtag_checked_none);
	$input->setLabelText(mi18n("value_none"));
	$td->setContent($input);
	$td->setClass("silverline");
	$input2 = new cHTMLRadiobutton("CMS_VAR[620]", 'below', '', $addtag_checked_below);
	$input2->setLabelText(mi18n("value_below"));
	$td->setContent($input."  ".$input2);
	$td->setClass("silverline");
	$fillrow[] = $td;
	$tr->setContent($fillrow);
	$filltable[] = $tr;
	
} // end if num_rows > 0
unset($db6);

 // Ende Tagging
 
 
//start next row with amount of articles
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_amountofarticles"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[110]", "CMS_VALUE[110]",'4','3','max_articleamount',false,null,'');
$td->setContent("Maximal:".$input);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with sort select
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_sort"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[115]");
$select->autofill(array( ""=>mi18n("please_choose"), "catart.idart"=>mi18n("Article ID"), "tblData.value"=>mi18n("Headline"), "artlang.created"=>mi18n("Add Date"),"artlang.lastmodified"=>mi18n("Last Modified Date"), "artlang.published"=>mi18n("Published Date"), "artlang.title"=>mi18n("Page Title (Backend)"), "artlang.pagetitle"=>mi18n("Page Title (Frontend)"), "artlang.summary"=>mi18n("Summary (Backend)"), "artlang.artsort"=>mi18n("Sort No."), "RAND()"=>mi18n("Random") ) );                
$select->setDefault("CMS_VALUE[115]");
$select2 = new cHTMLSelectElement("CMS_VAR[116]");
$select2->autofill(array( "ASC"=>mi18n("value_ascending"), "DESC"=>mi18n("value_descending") ) );                
$select2->setDefault("CMS_VALUE[116]");
$td->setContent($select.$select2);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with start article handling select
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_startarticle"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[120]");
$select->autofill(array( ""=>mi18n("nicht mit einschließen"), "startinclude"=>mi18n("mit einschließen"), "startonly"=>mi18n("ausschließlich Startartikel anzeigen") ) );                
$select->setDefault("CMS_VALUE[120]");
$td->setContent($select);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with THIS article include check
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_thisarticle"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$checkbox = new cHTMLCheckbox("CMS_VAR[123]", "true");
$checkbox->setLabelText( mi18n("nicht anzeigen. Aktuelle idart: ").$idart );
$checkbox->setChecked( "CMS_VALUE[123]" );
$td->setContent($checkbox);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//start next row with Template info
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_template"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[125]");

$option = new cHTMLOptionElement(mi18n("please_choose"), "");
$select->appendOptionElement($option);
foreach ($tplFiles AS $key=>$value) {
	$value = str_replace($tplpath, "", $value);

	$option = new cHTMLOptionElement($value, $value);
    if ("CMS_VALUE[125]" == $value) {
        $option->setSelected(true);
    }
    $select->appendOptionElement($option);
}

$td->setContent($select);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// final row with submit button only
$fillrow = array();
$tr = new cHTMLTableRow();
$tr->setClass("darkgray");
$td = new cHTMLTableData();
$td->setContent("");
$fillrow[] = $td;
$td = new cHTMLTableData();
$td->setContent($sSubmitLink);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// echo this fieldset, add legend and JS-functionality first
$table->setContent($filltable);

$fieldset = new cHTMLFieldset();
$fieldset->appendContent('<legend class="'.$faqclass.'"><h4><span class="morebutton '.$faqclass.'">&raquo;</span>'.mi18n("Main configuration").'</h4></legend>');
$fieldset->appendContent('<div class="answer">');
$fieldset->appendContent($table);
$fieldset->appendContent('</div>');
echo $fieldset->render();


//********************************
// start all over with fresh table
//********************************

$filltable = array();
$table = new cHTMLTable();

// next row with headline for article list
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_artlistheadline"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[301]", "CMS_VALUE[301]",'50','250','text_artlistheadline',false,null,'');
$td->setContent($input);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with nolist ARTICLE text
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_nolistarticle"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = fncBuildCategorySelect("CMS_VAR[306]", "", 0, "", 1, "", true, "CMS_VALUE[306]", false, true);
$td->setContent($select);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with nolist FREE text
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_nolistfreetext"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextarea("CMS_VAR[307]", "CMS_VALUE[307]", 40, 3);
$td->setContent($input);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//
$sOverviewLinkCat = (($iCount = substr_count("CMS_VALUE[311]",'cat_')) > 0) ? "CMS_VALUE[311]" : '';
$sOverviewLinkArt = (($iCount = substr_count("CMS_VALUE[311]",'art_')) > 0) ? "CMS_VALUE[311]" : '';

// next row with overview link
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_overviewlink")."*");
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = fncBuildCategorySelect("CMS_VAR[311]", $sOverviewLinkCat, 0, "", 1, "", true, $sOverviewLinkArt, false);
$td->setContent($select);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with date format and date sorting choices
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_date"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[316]");
$select->autofill(array(""=>mi18n("please_choose"), "d.m.y"=>"31.12.99", "d.m.Y"=>"31.12.1999", "d.m.y H:i"=>"31.12.99 13:30", "d.m.Y H:i"=>"31.12.1999 13:30", "F j, Y"=>mi18n("December")." 12, 1999", "M j, Y"=>mi18n("Dec")." 12, 1999", "j F Y"=>"31 ".mi18n("December")." 1999", "D, j M Y"=>mi18n("Fri").", 31 ".mi18n("Dec")." 1999", "Y-m-d"=>"1999-12-31" )  );       
$select->setDefault("CMS_VALUE[316]");
$select2 = new cHTMLSelectElement("CMS_VAR[317]");
$select2->autofill(array(""=>mi18n("please_choose"), "created"=>"created", "published"=>"published", "lastmodified"=>"lastmodified" )  );       
$select2->setDefault("CMS_VALUE[317]");
$td->setContent($select.$select2);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with pagination select
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_pagination"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = new cHTMLSelectElement("CMS_VAR[330]");
$select->autofill(array(""=>mi18n("none"), "1"=>"Style 1: Back / Next",   "2"=>"Style 2: 1 2 3 4 5 6 7 ...", "3"=>"Style 3: Page x of y", "4"=>"Style 4: &laquo; &lsaquo; 1 2 3 ... 5 6 7 ... 9 10 11 &rsaquo; &raquo;", "5"=>"Style 5:  &laquo; &lsaquo; 1 2 3 4 5 6 7 8 ... &rsaquo; &raquo;" )  );       
$select->setDefault("CMS_VALUE[330]");
$td->setContent($select);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with pagination additional specs
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent("");
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[331]", "CMS_VALUE[331]", 4, 3);
$input->setclass("follow");
$input2 = new cHTMLTextbox("CMS_VAR[332]", "CMS_VALUE[332]", 4, 3);
$input2->setclass("follow");
$td->setContent(mi18n("label_artperpage").$input. " ".mi18n("maximum_pages_for_style5").$input2);
$td->setClass("silverline");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

//new row with additional text changes settings
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_textchanges"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$checkbox = new cHTMLCheckbox("CMS_VAR[340]", "true");
$checkbox->setLabelText( mi18n("label_sub_textchanges") );
$checkbox->setChecked( "CMS_VALUE[340]" );
$td->setContent($checkbox);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with general information on additional strings (translation section)
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setColspan("2");
$td->setContent("* ".mi18n("info_addvalues_translation_section"));
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// final row with submit button only
$fillrow = array();
$tr = new cHTMLTableRow();
$tr->setClass("darkgray");
$td = new cHTMLTableData();
$td->setContent("");
$fillrow[] = $td;
$td = new cHTMLTableData();
$td->setContent($sSubmitLink);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// echo this fieldset, add legend and JS-functionality first
$table->setContent($filltable);

$fieldset = new cHTMLFieldset();
$fieldset->appendContent('<legend class="'.$faqclass.'"><h4><span class="morebutton  '.$faqclass.'">&raquo;</span>'.mi18n("Additional configuration").'</h4></legend>');
$fieldset->appendContent('<div class="answer">');
$fieldset->appendContent($table);
$fieldset->appendContent('</div>');
echo $fieldset->render();

//********************************
// start all over with fresh table
//********************************

$filltable = array();
$table = new cHTMLTable();

// next row with sample article tree
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_samplearticle"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = fncBuildCategorySelect("CMS_VAR[500]", "", 0, "", 1, "", true, "CMS_VALUE[500]", '', true);
$td->setContent($select.$sSubmitLink);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with headline article tree
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_artheadline"));
$td->setClass("silverlinetop");
$fillrow[] = $td;
$td = new cHTMLTableData();
$select = fncBuildTypeSelect("CMS_VAR[510]", "CMS_VALUE[500]", "CMS_VALUE[510]", "'1','2','3','9','17'");
$input = new cHTMLTextbox("CMS_VAR[511]", "CMS_VALUE[511]", 4, 3);
$input->setclass("follow");
$td->setContent($select." ".mi18n("length_max").$input);
$td->setClass("silverlinetop");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with summary use option
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_summary"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[515]", "CMS_VALUE[515]", 4, 3);
$checkbox = new cHTMLCheckbox("CMS_VAR[516]", "true");
$checkbox->setLabelText( mi18n("use_summary_as_headline") );
$checkbox->setChecked( "CMS_VALUE[516]" );
$checkbox->setclass("follow2");
$td->setContent($input.mi18n("length").$checkbox);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with page title use option
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_pagetitle"));
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[520]", "CMS_VALUE[520]", 4, 3);
$checkbox = new cHTMLCheckbox("CMS_VAR[521]", "true");
$checkbox->setLabelText( mi18n("use_pagetitle_as_headline") );
$checkbox->setChecked( "CMS_VALUE[521]" );
$checkbox->setclass("follow2");
$td->setContent($input.mi18n("length").$checkbox);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

// next row with number of additional elements
$fillrow = array();
$tr = new cHTMLTableRow();
$td = new cHTMLTableData();
$td->setContent(mi18n("label_addelements"));
$td->setClass("silverlinetop");
$fillrow[] = $td;
$td = new cHTMLTableData();
$input = new cHTMLTextbox("CMS_VAR[525]", "CMS_VALUE[525]", 4, 3);
$td->setContent($input.$sSubmitLink);
$td->setClass("silverlinetop");
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;

$elements = "CMS_VALUE[525]";
$samplearticle = "CMS_VALUE[500]";

if ( $elements > 0) {
    for ($i = 1; $i <= $elements; $i++) {
	    
		$iElementType       = $iDataStart + ($i * 10);
		$iElementIdentifier     = $iElementType + 1;
		$iElementWidth          = $iElementType + 2;
		$iElementHeight         = $iElementType + 3;
		$iElementDontStripTags  = $iElementType + 3;
		$iElementAllowedTags    = $iElementType + 4;

		//new row with text output only for this element
		$fillrow = array();
		$tr = new cHTMLTableRow();
		$td = new cHTMLTableData();
		$td->setContent(mi18n("%s. Element", $i));
		$td->setClass("silverlinetop");
		$fillrow[] = $td;
		$td = new cHTMLTableData();
		$td->setContent(mi18n("Type"));
		$td->setClass("silverlinetop");
		$fillrow[] = $td;
		$tr->setContent($fillrow);
		$filltable[] = $tr;
		
		//new row with content type select
		$fillrow = array();
		$tr = new cHTMLTableRow();
		$td = new cHTMLTableData();
		$td->setContent("");
		$fillrow[] = $td;
		$td = new cHTMLTableData();
		$select = new cHTMLSelectElement("CMS_VAR[$iElementType]");
		$select->autofill(array("Text"=>mi18n("Text"), "Image"=>"Image",   "ExtractedImage"=>"Image from text", "FixedDate"=>"Fixed date" )  );       
		$select->setDefault("CMS_VALUE[$iElementType]");
		$td->setContent($select.$sSubmitLink);
		$fillrow[] = $td;
		$tr->setContent($fillrow);
		$filltable[] = $tr;	   

		
		//switch ($aSettings['k'.$i]['ElementType']) {
		switch ("CMS_VALUE[$iElementType]") {
		    case "Image":
			    
			// next row with image container select
			$fillrow = array();
			$tr = new cHTMLTableRow();
			$td = new cHTMLTableData();
			$td->setContent("");
			$fillrow[] = $td;
			$td = new cHTMLTableData();
			$select = fncBuildTypeSelect("CMS_VAR[$iElementIdentifier]", $samplearticle, "CMS_VALUE[$iElementIdentifier]", "'4','22'");
			$input = new cHTMLTextbox("CMS_VAR[$iElementWidth]", "CMS_VALUE[$iElementWidth]", 4, 10);
			$input->setclass("follow");
			$input2 = new cHTMLTextbox("CMS_VAR[$iElementHeight]", "CMS_VALUE[$iElementHeight]", 4, 10);
			$input2->setclass("follow");
			$td->setContent($select." ".mi18n("width_max").$input.mi18n("height_max").$input2);
			$fillrow[] = $td;
			$tr->setContent($fillrow);
			$filltable[] = $tr;	    
		    
			break;
		    case "ExtractedImage":

			// next row with imageExtract container select
			$fillrow = array();
			$tr = new cHTMLTableRow();
			$td = new cHTMLTableData();
			$td->setContent("");
			$fillrow[] = $td;
			$td = new cHTMLTableData();
			$select = fncBuildTypeSelect("CMS_VAR[$iElementIdentifier]", $samplearticle,  "CMS_VALUE[$iElementIdentifier]", "'1','2','3','9','17'");
			$input = new cHTMLTextbox("CMS_VAR[$iElementWidth]", "CMS_VALUE[$iElementWidth]", 4, 10);
		    	$input->setclass("follow");
			$input2 = new cHTMLTextbox("CMS_VAR[$iElementHeight]", "CMS_VALUE[$iElementHeight]", 4, 10);
			$input2->setclass("follow");
			$td->setContent($select." ".mi18n("width_max").$input.mi18n("height_max").$input2);
			$fillrow[] = $td;
			$tr->setContent($fillrow);
			$filltable[] = $tr;	 

			break;
		    case "FixedDate":
			    
			// next row with date container select
			$fillrow = array();
			$tr = new cHTMLTableRow();
			$td = new cHTMLTableData();
			$td->setContent("");
			$fillrow[] = $td;
			$td = new cHTMLTableData();
			$select = fncBuildTypeSelect("CMS_VAR[$iElementIdentifier]", $samplearticle,  "CMS_VALUE[$iElementIdentifier]", "'19'");
			$td->setContent($select);
			$fillrow[] = $td;
			$tr->setContent($fillrow);
			$filltable[] = $tr;	
		    
			break;
		    default:

			// next row with text and other container select
			$fillrow = array();
			$tr = new cHTMLTableRow();
			$td = new cHTMLTableData();
			$td->setContent("");
			$fillrow[] = $td;
			$td = new cHTMLTableData();
			$select = fncBuildTypeSelect("CMS_VAR[$iElementIdentifier]", $samplearticle,  "CMS_VALUE[$iElementIdentifier]", "'1','2','3','9','17'");
			$input = new cHTMLTextbox("CMS_VAR[$iElementWidth]", "CMS_VALUE[$iElementWidth]", 4, 10);
			$input->setclass("follow");
			$td->setContent($select.mi18n("length_max").$input);
			$fillrow[] = $td;
			$tr->setContent($fillrow);
			$filltable[] = $tr;	

			// next row with text tag specs
			$fillrow = array();
			$tr = new cHTMLTableRow();
			$td = new cHTMLTableData();
			$td->setContent("");
			$fillrow[] = $td;
			$td = new cHTMLTableData();
			$checkbox = new cHTMLCheckbox("CMS_VAR[$iElementDontStripTags]", "true");
			$checkbox->setLabelText( mi18n("keep_tag_question") );
			$checkbox->setChecked( "CMS_VALUE[$iElementDontStripTags]" );
			$input = new cHTMLTextbox("CMS_VAR[$iElementAllowedTags]","CMS_VALUE[$iElementAllowedTags]", 40, 100);
			$input->setclass("follow");
			$td->setContent($checkbox."<br>".mi18n("allowed_tags")." ('&lt;a&gt;,&lt;h1&gt;' etc.):".$input);
			$fillrow[] = $td;
			$tr->setContent($fillrow);
			$filltable[] = $tr;	
			
		}		
		
		

	}
}


// final row with submit button only
$fillrow = array();
$tr = new cHTMLTableRow();
$tr->setClass("darkgray");
$td = new cHTMLTableData();
$td->setContent("");
$fillrow[] = $td;
$td = new cHTMLTableData();
$td->setContent($sSubmitLink);
$fillrow[] = $td;
$tr->setContent($fillrow);
$filltable[] = $tr;


// echo this fieldset, add legend and JS-functionality first
$table->setContent($filltable);

$fieldset = new cHTMLFieldset();
$fieldset->appendContent('<legend class="'.$faqclass.'"><h4><span class="morebutton '.$faqclass.'">&raquo;</span>'.mi18n("Identify Elements").'</h4></legend>');
$fieldset->appendContent('<div class="answer">');
$fieldset->appendContent($table);
$fieldset->appendContent('</div>');
echo $fieldset->render();



?><?php