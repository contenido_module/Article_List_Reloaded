<?php
/**
 * Project: 
 * Contenido Content Management System
 * 
 * Description: 
 * Class for Article list advanced modul
 * 
 * Requirements: 
 * @con_php_req 5.2
 * 
 *
 * @package    Contenido Frontend classes
 * @version    1.0.0
 * @author     Tiago Gomes
 * @copyright  Tiago Gomes <www.tiago.de>
 * @license    
 * @link       http://www.tiago.de
 * @link       http://forum.contenido.org/viewtopic.php?p=134520#p134520
 * @since      file available since contenido release 4.8.12
 * @deprecated
 * @class      DateTimeReplaceLang
 * @brief      
 * @file       class.datetimereplacelang.php
 * @date       2010-08-20
 * 
 * {@internal 
 *   created 2010
 *   modified xxxx-xx-xx, [Modifier Name], [Description]
 *   ...
 *
 * }}
 * 
 */

if(!defined('CON_FRAMEWORK')) {
	die('Illegal call');
}

class DateTimeReplaceLang extends DateTime {
	public function format($format, $replace = 'de_DE', $type = 'short') {
		if ($type == 'long')
                    $aEnglish = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                else
                    $aEnglish = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

		switch ($replace) {
			case 'pt_PT':
				if ($type == 'long')
                                    $aReplace = array('Segundafeira', 'Ter&ccedil;afeira', 'Quartafeira', 'Quintafeira', 'Sextafeira', 'S&aacute;bado', 'Domingo', 'Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
                                else
                                    $aReplace = array('2<sup>a</sup>feira', '3<sup>a</sup>feira', '4<sup>a</sup>feira', '5<sup>a</sup>feira', '6<sup>a</sup>feira', 'S&aacute;bado', 'Domingo', 'Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
				break;
			case 'de_DE':
				if ($type == 'long')
                                    $aReplace = array('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag', 'Januar', 'Februar', 'M&auml;rz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember');
                                else
                                    $aReplace = array('Mo', 'Di', 'Mi', 'Do', 'Fri', 'Sa', 'So', 'Jan', 'Feb', 'M&auml;r', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez');
				break;
			case 'es_ES':
				if ($type == 'long')
                                    $aReplace = array('Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
                                else
                                    $aReplace = array('Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb', 'Dom', 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic');
				break;
			case 'ca_CA':
				if ($type == 'long')
                                    $aReplace = array('dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge', 'gener', 'febrer', 'març', 'abril', 'maig', 'juny', 'juliol', 'agost', 'setembre', 'octubre', 'novembre', 'desembre');
                                else
                                    $aReplace = array('Mo', 'Di', 'Mi', 'Do', 'Fri', 'Sa', 'So', 'gen.', 'febr.', 'març', 'abr.', 'maig', 'juny', 'jul.', 'ag.', 'set.', 'oct.', 'nov.', 'des.');
				break;				
			default:
				if ($type == 'long')
                                    $aReplace = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                                else
                                    $aReplace = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
				break;
		}
//                print ' E#### ';
//                print_r($aEnglish);
//                print ' R#### ';
//                print_r($aReplace);
//                print str_replace($aEnglish, $aReplace, parent::format($format));
//                print '#'.parent::format($format).'#';
		return str_replace($aEnglish, $aReplace, parent::format($format));
	}
}
?>