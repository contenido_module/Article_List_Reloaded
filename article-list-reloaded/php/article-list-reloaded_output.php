<?php
// Includes
//removed functions.include.UTF8decoding.php, as was only needed for PHP <= 4.
//removed older versions of data replacement and php check, as CON 4.9 is not running with php lower than 5.2.3
//moved all include-files to module/php folder which makes transfering this module easier
cInclude('module', 'class.datetimereplacelang.php');
cInclude('module', 'functions.alr.php');

//in case thumbs are not displayed properly, this might help:
putenv("MAGICK_THREAD_LIMIT=1");

// initialize
$tpl = cSmartyFrontend::getInstance();
$articleList = array();
$listObj = new stdClass();

// Prepare module info. Needed later to check if defined default smarty template is a real file 
$mod = new cApiModule($cCurrentModule);
$tplpath = $cfgClient[$client]["path"]["frontend"]."data/modules/".$mod->get("alias")."/template/";

$bDebug     = false;
$iDataStart = 1000; // Startwert fuer dynamisch generierte CMS_VAR Elemente !!!Muss im Input gleich sein!!!
$lCount     = 0;

// Base settings
/**
* Base information:
* $aSettings['ArtListHeadline']:           Value for article list headline
* $aSettings['Elements']:                  Number of expected, additional elements from each article
* $aSettings['ArticlePerPage']:            Number of articles per page (0: show all)
* $aSettings['k'.$i]['ElementType']:       Per Element: Type of Element: Text, Image or ExtractedImage
* $aSettings['k'.$i]['ElementWidth']:      Per Element: Text length or image width (-> recycling ...)
* $aSettings['k'.$i]['ElementHeight']:     Per Element: Image width
* $aSettings['k'.$i]['DontStripTags']      Per Element: Strip tags or not
* $aSettings['k'.$i]['ElementAllowedTags'] Per Element: Allowed tags
*/

unset ($aSettings);
$aSettings = array();
$oLang = new cApiLanguage();
$oLang->loadByPrimaryKey($lang);
$aSettings['DateLanguage'] = $oLang->getProperty("dateformat", "locale");
$aSettings['ShowModul']                  = ( "CMS_VALUE[0]" == 'true' ) ? true : false ;
$aSettings['Category']                   = str_replace('cat_', '', "CMS_VALUE[100]" );
$aSettings['Category']                   = ( $aSettings['Category'] <= 0 )  ? 0 : $aSettings['Category'];
$aSettings['AddCats']                    = str_replace('cat_','', "CMS_VALUE[106]");
$aSettings['CatTypeSel']                 = ( "CMS_VALUE[105]" == '' ||  ($aSettings['AddCats'] == '' && "CMS_VALUE[105]" == 'selected') )  ? 'none' : "CMS_VALUE[105]"; 
$aSettings['ArticleCount']               = ( "CMS_VALUE[110]" < 0)     ? 0 : "CMS_VALUE[110]";
$aSettings['SortBy']                     = ( "CMS_VALUE[115]" == '' )     ? "catart.idart" : "CMS_VALUE[115]";
$aSettings['SortDir']                    = "CMS_VALUE[116]";
$aSettings['ShowStart']                  = ( "CMS_VALUE[120]" == 'startinclude' ) ? true : false ;
$aSettings['ShowOnlyStart']              = ( "CMS_VALUE[120]" == 'startonly' ) ? true : false ;
$aSettings['ShowCurrentArticle']         = ( "CMS_VALUE[123]" == 'true' ) ? true : false ;
$aSettings['ArtListHeadline']            = "CMS_VALUE[301]";
$aSettings['DefaultTextArticle']         = str_replace('art_', '', "CMS_VALUE[306]");
$aSettings['DefaultTextFree']            = "CMS_VALUE[307]";
$aSettings['OverviewLink']               = "CMS_VALUE[311]";
$aSettings['OverviewLinkText']           = ( "CMS_VALUE[312]" == "") ? mi18n("Link to overview") : "CMS_VALUE[312]" ;
$aSettings['DateFormat']                 = ( "CMS_VALUE[316]" == "") ? "d.m.Y" : "CMS_VALUE[316]" ;
$aSettings['DateChoice']                  = "CMS_VALUE[317]";
$aSettings['PaginationType']             = "CMS_VALUE[330]";
$aSettings['ArticlePerPage']             = ( $aSettings['PaginationType'] != ''  && "CMS_VALUE[331]" >= 2)  ? "CMS_VALUE[331]" : 1;
$aSettings['PaginationType05MaxPages']   = ( !is_numeric("CMS_VALUE[332]") || "CMS_VALUE[332]" == '' || "CMS_VALUE[332]" <= 3 ) ? 3 : "CMS_VALUE[332]";
$aSettings['TextConversion']            = ( "CMS_VALUE[340]" == 'true' ) ? true : false ;
$aSettings['HeadlineIdentifier']         = "CMS_VALUE[510]";
$aSettings['HeadlineLength']             = ( !is_numeric("CMS_VALUE[511]") || "CMS_VALUE[511]" < 0) ? 0 : "CMS_VALUE[511]"; 
$aSettings['UseSummary']                 = ( "CMS_VALUE[516]" == 'true' ) ? true : false ;
$aSettings['SummaryWidth']               = ( !is_numeric("CMS_VALUE[515]") || "CMS_VALUE[515]" < 0)     ? 0 : "CMS_VALUE[515]";
$aSettings['UsePageTitle']               = ( "CMS_VALUE[521]" == 'true' ) ? true : false ;
$aSettings['PageTitleWidth']             = ( !is_numeric("CMS_VALUE[520]") || "CMS_VALUE[520]" < 0) ? 0 : "CMS_VALUE[520]";
$aSettings['Elements']                   = ( !is_numeric("CMS_VALUE[525]") || "CMS_VALUE[525]" < 0) ? 0 : "CMS_VALUE[525]";
$aSettings['CurrentPage']                = ( !is_numeric($_REQUEST['page']) || !isset($_REQUEST['page']) ) ? 1 : $_REQUEST['page'];
if ( $aSettings['ShowOnlyStart'] )  $aSettings['ShowStart']  = true; 
$aSettings['Template']                   = "CMS_VALUE[125]";
$aSettings['TaggingCategory']            = "CMS_VALUE[610]"; // Tagging
$sSelectedPica = $aSettings['TaggingCategory']; // Tagging
$aSettings['TaggingMoreCategories']      = "CMS_VALUE[620]"; // Tagging

if ( $aSettings['ShowModul']  ) {
    
    // Client Cache loeschen um auf jeden Fall alle Aenderungen an Inhalten zu beruecksichtigen
    // Es wird explizit nur der content-Ordner geloescht um zu verhindern, dass es Fehler bei der Darstellung von Bildern gibt
    cInclude('classes', 'class.purge.php');
    $oPurge = new cSystemPurge($db, $cfg, $cfgClient);
    $oPurge->clearClientCache($iClientId, 'cache/content/');
    
	// Detail settings
    if ($aSettings['Elements'] > 0) {
        for ($i = 1; $i <= $aSettings['Elements']; $i++) {
	    $iElementType           = $iDataStart   + ($i * 10);
	    $iElementIdentifier     = $iElementType + 1;
	    $iElementWidth          = $iElementType + 2;
	    $iElementHeight         = $iElementType + 3;
	    $iElementDontStripTags  = $iElementType + 3;
	    $iElementAllowedTags    = $iElementType + 4;

	    $aSettings['k'.$i] = array();
	    $aSettings['k'.$i]['ElementType']        = "CMS_VALUE[$iElementType]"; // Text, Image, ExtractedImage, FixedDate
	    $aSettings['k'.$i]['Identifier']         = "CMS_VALUE[$iElementIdentifier]";
		// Der obige Wert muss hier nochmal umgebogen werden, weil im Inputbereich der QueryString in der functions.input.helpr.gw.php leider geÃ¤ndert werden musste.
		$aSettings['k'.$i]['Identifier']         = str_replace("tblDataidtype", "tblData.idtype = '", $aSettings['k'.$i]['Identifier']);
		$aSettings['k'.$i]['Identifier']         = str_replace("tblDatatypeid", "' AND tblData.typeid = '", $aSettings['k'.$i]['Identifier']);
		$aSettings['k'.$i]['Identifier']         = $aSettings['k'.$i]['Identifier']."'";
	    $aSettings['k'.$i]['ElementWidth']       = "CMS_VALUE[$iElementWidth]";       // Textlength or ImageWidth
	    $aSettings['k'.$i]['ElementHeight']      = "CMS_VALUE[$iElementHeight]";
	    $aSettings['k'.$i]['DontStripTags']      = ("CMS_VALUE[$iElementDontStripTags]" == 'true' ) ? true : false ;
	    $aSettings['k'.$i]['ElementAllowedTags'] = ("CMS_VALUE[$iElementAllowedTags]" != '') ? '\''."CMS_VALUE[$iElementAllowedTags]".'\'' : '';

	    // Check detail settings
	    if ($aSettings['k'.$i]['ElementType'] == '') $aSettings['k'.$i]['ElementType'] == 'Text';

	    if ($aSettings['k'.$i]['ElementType'] == 'Text') {
		$aSettings['ElementHeight'] = 0;
		if (!is_numeric($aSettings['k'.$i]['ElementWidth']) || $aSettings['k'.$i]['ElementWidth'] < 0) {
		    $aSettings['k'.$i]['ElementWidth'] = 0;
		}
	    } elseif (!is_numeric($aSettings['k'.$i]['ElementWidth']) || $aSettings['k'.$i]['ElementWidth'] <= 0 ||
		    !is_numeric($aSettings['k'.$i]['ElementHeight']) || $aSettings['k'.$i]['ElementHeight'] <= 0) {
		$aSettings['k'.$i]['ElementWidth'] = 0;
		$aSettings['k'.$i]['ElementHeight'] = 0;
	    }

	    if (strlen($aSettings['Identifier']) > 50) $aSettings['Identifier'] = '';
	    
        } // end for
    }

    unset ($iDataStart);
    unset ($iElementType);
    unset ($iElementIdentifier);
    unset ($iElementWidth);
    unset ($iElementHeight);
    unset ($iElementDontStripTags);
    unset ($iElementAllowedTags);

    //if ($aSettings['Category'] !== '0' && $aSettings['HeadlineIdentifier'] !== '') {
   if ($aSettings['Category'] !== '0') {
	   	   
	   $db2 = cRegistry::getDb();

        // Specifying search categories
        $sSelCats = '"'.$aSettings['Category'].'"';
        // Adding categories "below" primary category
        switch ($aSettings['CatTypeSel']) {
            case 'below':
                $lCatLevel = -1;
                $sql  = 'SELECT'."\n".
                        '   a.idcat AS idcat,'."\n".
                        '   b.level AS level'."\n".
                        'FROM'."\n".
                        '   '.$cfg['tab']['cat'].' a,'."\n".
                        '   '.$cfg['tab']['cat_tree'].' b'."\n".
                        'WHERE'."\n".
                        '   a.idcat = b.idcat'."\n".
                        'ORDER BY'."\n".
                        '   b.idtree';

                $db2->query($sql);
                while($db2->nextRecord()) {
                    if ($db2->f('idcat') == $aSettings['Category'])
                        $lCatLevel = $db2->f('level');
                    elseif ($lCatLevel > -1 && $db2->f('level') > $lCatLevel)
                        $sSelCats .= ',"'.$db2->f('idcat').'"';
                    elseif ($db2->f('level') <= $lCatLevel)
                        break;
                } // end while
                break;
            case 'select':
                $aCats = explode(",", $aSettings["AddCats"] ); // http://forum.contenido.org/viewtopic.php?p=149395#p149395
                foreach ($aCats as $value)
                    if (is_numeric($value))
                        $sSelCats .= ',"'.$value.'"';
                break;
            default:
        } // end switch
	
	// prepare alternative inputs:
	if ($aSettings['HeadlineIdentifier']  != '' ) {

		$aSettings['HeadlineIdentifier'] = str_replace("tblDataidtype", "tblData.idtype = '", $aSettings['HeadlineIdentifier']);
		$aSettings['HeadlineIdentifier'] = str_replace("tblDatatypeid", "' AND tblData.typeid = '", $aSettings['HeadlineIdentifier']);
		
		$addsql01 = '    tblData.value AS headline,'."\n";
		$addsql02 = $cfg['tab']['content'].' AS tblData,'."\n";
		$addsql04 = 'AND '."\n".'    tblData.idartlang != catlang.startidartlang ';
		$addsql05 = 'AND '."\n".'    tblData.idartlang = catlang.startidartlang ';
		
	}
	if ($aSettings['TaggingCategory']) { 
	// tagging needs additional and/or other statements - compile them here
	
	$sql = "SELECT online FROM ".$cfg["tab"]["pica_lang"]." WHERE idpica_alloc=$sSelectedPica";
	$db->query($sql);
	$db->nextRecord();
	if ($db->f("online") == 0)
	$sSelectedPica = 0;

	// Getting all relevant allocations:
	if ($aSettings['TaggingMoreCategories'] == "below" && $sSelectedPica != 0) {
		$picas = "($sSelectedPica".getChildPicas($sSelectedPica).")";
		}
		else {
		$picas = "($sSelectedPica)";
	}

	// additional SELECT and from statements for Content Allocation / Tagging

		$addsqlpicaselect = '    pica.idpica_alloc,'."\n".
		'    pica.idartlang,'."\n".
		'    picalang.name,'."\n";

		$addsqlpicafrom = '    '.$cfg['tab']['pica_alloc_con'].' AS pica,'."\n".
		'    '.$cfg['tab']['pica_lang'].' AS picalang,'."\n";

		// different WHERE part depending if Article List or Content Allocation / Tagging
		$sqlwhere = '    pica.idpica_alloc IN '.$picas.' AND'."\n".
		'    pica.idartlang = artlang.idartlang AND'."\n".
		'    pica.idpica_alloc = picalang.idpica_alloc AND'."\n".
		'    picalang.idlang = artlang.idlang AND'."\n";

		} else {

			$addsqlpicaselect = ''; 
			$addsqlpicafrom = ''; 
		$sqlwhere = 		'    catart.idcat IN ('.$sSelCats.') AND'."\n";

	};

	// end additional SQL for Tagging

        $sql =  'SELECT'."\n".
                '    artlang.author AS author,'."\n".
                $addsql01.
                '    artlang.pagetitle AS pagetitle,'."\n".
                '    artlang.summary AS summary,'."\n".
                '    artlang.idart AS idart,'."\n".
                '    artlang.idart AS idartlang,'."\n".
                '    artlang.lastmodified AS lastmodified,'."\n".
                '    artlang.modifiedby AS autor,'."\n".
                '    artlang.created AS created,'."\n".
                '    artlang.published AS published,'."\n".
                '    artlang.idartlang AS idartlang,'."\n".
                '    catlang.name AS category,'."\n".
                '    catlang.startidartlang AS startid,'."\n".
                '    catart.is_start AS isstart,'."\n".		
		$addsqlpicaselect. // Tagging
                '    catart.idcat AS idcat,'."\n".
                '    catart.idcatart AS idcatart'."\n".
                'FROM'."\n".
                '    '.$cfg['tab']['cat_art'].' AS catart,'."\n".
                '    '.$cfg['tab']['art_lang'].' AS artlang,'."\n".
                $addsql02.
		$addsqlpicafrom. // Tagging
                '    '.$cfg['tab']['cat_lang'].' AS catlang'."\n".
                'WHERE'."\n".
		$sqlwhere. // Tagging
                // '    catart.idcat IN ('.$sSelCats.') AND'."\n". // statement built in by tagging now if required
                '    artlang.idlang = '.$lang.' AND'."\n".
                //$addsql03.
                '    catlang.idlang = artlang.idlang AND'."\n".
                '    catlang.idcat = catart.idcat AND'."\n".
                '    artlang.idart = catart.idart AND'."\n".
                '    artlang.online = 1 ';

        if ( !$aSettings['ShowStart'] )         $sql .=  'AND '."\n".'    artlang.idartlang != catlang.startidartlang ' . $addsql04 ;
        if ( $aSettings['ShowOnlyStart'] )      $sql .=  'AND '."\n".'    artlang.idartlang = catlang.startidartlang '  .$addsql05  ;
        if ( $aSettings['ShowCurrentArticle'] )  $sql .= 'AND'."\n".'    artlang.idart <> ' . $idart . ' ';

	if ($aSettings['HeadlineIdentifier']  != '' ) {
		$sql .=  'AND    tblData.idartlang = artlang.idartlang '."\n";
		$sql .=  'AND '."\n".$aSettings['HeadlineIdentifier']."'";
	}

	//rethus Bugfix: http://forum.contenido.org/viewtopic.php?f=89&t=34196&p=163601#p163601 to avoid redundant article inclusion if article limit > available articles
	$sql .= "\n".'GROUP BY idart'."\n";
        // Sort by
        $sql .= "\n".'ORDER BY'."\n".
                '    '.$aSettings['SortBy']."\n".
                '    '.$aSettings['SortDir']."\n";

        // LIMIT
        if ($aSettings['ArticleCount'] > 0) $sql .= 'LIMIT 0, '.$aSettings['ArticleCount'];
		
        if ($bDebug) {
            echo '<pre>'.$sql.'</pre>', chr(10);
        }

        // execute query
        $db2->query($sql);
	
        $lCount = $db2->numRows();

        if ($lCount > 0) {
            if (!is_object($db3)) $db3 = cRegistry::getDb();
            if (!is_object($db4)) $db4 = cRegistry::getDb();
            if (!is_object($db5)) $db5 = cRegistry::getDb();
	    
	    //falsche GET-Werte für "page" auf 0 setzen, Kurzcheck:
	    if ( $aSettings['CurrentPage'] > ceil( $lCount / $aSettings['ArticlePerPage'] )  ) $aSettings['CurrentPage'] = 1;

            if ($aSettings['ArticlePerPage'] > 0 && $aSettings['PaginationType'] != '' ) {
                $lStartCount = ($aSettings['CurrentPage'] - 1) * $aSettings['ArticlePerPage'];
                $lEndCount   = (($aSettings['CurrentPage'] - 1) * $aSettings['ArticlePerPage']) + $aSettings['ArticlePerPage'];

                if ($lEndCount > $lCount) $lEndCount = $lCount;
            } else {
                $lStartCount = 0;
                $lEndCount   = $lCount;
            }

            $lRow = 0;
            $i    = 0; 	    
    
            while ($db2->nextRecord()) 
	{

		if ($lRow >= $lStartCount && $lRow < $lEndCount)
		{
			$article = array();
			//generate first values

			$article['number']     = $lRow+1 ; 
			$article['category']   = urldecode($db2->f('category')); 
                        $article['categoryid'] = urldecode($db2->f('idcat')); 
                        $article['articleid'] = urldecode($db2->f('idart')); 

			//article link, relative to get to the listed article
                        if ($db2->f('startid') == $db2->f('idartlang') || $db2->f('isstart')) {
                            $aParams = array('lang' => $lang, 'idcat' => $db2->f('idcat'));
                        } else {
                            $aParams = array('lang' => $lang, 'idcat' => $db2->f('idcat'), 'idart' => $db2->f('idart'));
                        }
                        $article['link'] = cUri::getInstance()->buildRedirect($aParams);

                        // new: create link to category in which article is located in
                        $aParams = array('lang' => $lang, 'idcat' => $db2->f('idcat'));
                        $article['linkcat'] = cUri::getInstance()->buildRedirect($aParams);

			//Generate ALL date values possible and store them in array
			$oDate = new DateTimeReplaceLang( $db2->f('lastmodified') );
			$article['lastmodified_long']   = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'long');
			$article['lastmodified_short']  = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'short');

			$oDate = new DateTimeReplaceLang( $db2->f('created') );
			$article['created_long']   = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'long');
			$article['created_short']  = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'short');
			
			$oDate = new DateTimeReplaceLang( $db2->f('published') );
			$article['published_long']  = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'long');
			$article['published_short'] = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'short');
			
			$article['published'] = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article['published_long'] : $article['published_short'];
			$article['created'] = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article['created_long'] : $article['created_short'];
			$article['lastmodified'] = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article['lastmodified_long'] : $article['lastmodified_short'];

			$article['date']  = ($aSettings['DateChoice'] != '') ? $article[$aSettings['DateChoice']]  : ''; 

			//get author's real name
			$author = new cApiUser();
			$author->loadUserByUsername( $db2->f('author'));
			$article["author"] = text2adapt(  $author->getRealName( ),  false, $aSettings['TextConversion']  );			
			
			// Generate Summary
			$article['summary_full']  = text2adapt( $db2->f('summary') , false , $aSettings['TextConversion'] );
			$article['summary_trimmed']  = cString::trimAfterWord($article['summary_full'], $aSettings['SummaryWidth']).'...';
			$article['summary']  = ($aSettings['SummaryWidth'] > 0 && strlen($article['summary_full'] ) > $aSettings['SummaryWidth'] ) ? $article['summary_trimmed']  : $article['summary_full'] ;

                        // Generate pagetitle
			$article['pagetitle_full'] = text2adapt( $db2->f('pagetitle') , false , $aSettings['TextConversion'] );
			$article['pagetitle_trimmed'] = cString::trimAfterWord($article['pagetitle_full'], $aSettings['PageTitleWidth']).'...';
                        $article['pagetitle'] = ($aSettings['PageTitleWidth'] > 0 && strlen($article['pagetitle_full'] ) > $aSettings['PageTitleWidth'] ) ? $article['pagetitle_trimmed'] : $article['pagetitle_full'] ;

                        // Generate headline
			$article['headline_full'] = text2adapt( $db2->f('headline') , false , $aSettings['TextConversion'] );
			$article['headline_trimmed'] = cString::trimAfterWord($article['headline_full'], $aSettings['HeadlineLength']).'...';
			$article['headline']  = ($aSettings['HeadlineLength'] > 0 && strlen($article['headline_full'] ) > $aSettings['HeadlineLength']) ? $article['headline_trimmed'] : $article['headline_full'] ;
			$article['headline_stripped']  = strip_tags($article['headline']) ;

			// Replace headline with summary or page title if clicked
			if ( $aSettings['UseSummary'] ) $article['headline'] = $article['summary'] ;
			if ( $aSettings['UsePageTitle'] ) $article['headline'] = $article['pagetitle'] ;
		
			
                        //go through all chosen elements defined by user one by one
                        if ($aSettings['Elements'] > 0) {

                            $sql =  'SELECT'."\n".
                                    '    tblData.value AS value,'."\n".
                                    '    tblData.idtype AS idtype,'."\n".
                                    '    tblData.typeid AS typeid'."\n".
                                    'FROM'."\n".
                                    '    '.$cfg['tab']['cat_art'].' AS tblCatArt,'."\n".
                                    '    '.$cfg['tab']['art_lang'].' AS tblArtLang,'."\n".
                                    '    '.$cfg['tab']['content'].' AS tblData'."\n".
                                    'WHERE'."\n".
                                    '    tblData.idartlang = tblArtLang.idartlang AND'."\n".
                                    '    tblArtLang.idlang = "'.$lang.'" AND'."\n".
                                    '    tblArtLang.idart = tblCatArt.idart AND'."\n".
                                    '    tblCatArt.idcatart = "'.$db2->f('idcatart').'" AND'."\n".
                                    '    ('."\n";

                            $sql_items = '';
                            for ($k = 1; $k <= $aSettings['Elements']; $k++) {
                                if ($aSettings['k'.$k]['Identifier'] != '' && $aSettings['k'.$k]['Identifier'] != 'none') {
                                    if ($sql_items != '' ) {
                                        $sql_items .= ' OR'."\n".
                                                      '        ('.$aSettings['k'.$k]['Identifier'].')';
                                    } else {
                                        $sql_items = '        ('.$aSettings['k'.$k]['Identifier'].')';
                                    }
                                }
                            } // end for
                            $sql .= $sql_items."\n".'    )';

                            if ($bDebug) {
                                echo '<pre>'.$sql.'</pre>', chr(10);
                            }

                            // execute query
                            $db3->query($sql);

                            while ($db3->nextRecord()) {
                                $sTypeIdentifier = "tblData.idtype = '".$db3->f('idtype')."' AND tblData.typeid = '".$db3->f('typeid')."'";

                                /**
                                 * Note: The TypeIdentifier for one item may be the same as for another item.
                                 * Therefore, we are storing the content everywhere as needed
                                 */
                                for ($k = 1; $k <= $aSettings['Elements']; $k++) { 
                                    if ($sTypeIdentifier == $aSettings['k'.$k]['Identifier']) {
                                        switch (TRUE) {
                                            case ($aSettings['k'.$k]['ElementType'] == 'Image' || $aSettings['k'.$k]['ElementType'] == 'ExtractedImage'):

                                                $sql = ''; 
						unset($ChosenImage); // corr von chris8408
						
                                                if ($aSettings['k'.$k]['ElementType'] == 'Image') { 
                                                    $sql =  'SELECT idupl, dirname, filename FROM '.$cfg['tab']['upl']. ' WHERE idupl = "'.$db3->f('value').'"';
                                                    $db4->query($sql); 

                                                    if ($db4->nextRecord()) {
														$ChosenImage[path] = $db4->f('dirname');
														$ChosenImage[name] = $db4->f('filename');
														
													}
                                                } else {
                                                    $sTmpValue = urldecode($db3->f('value'));
													
													$doc=new DOMDocument();
													$doc->loadHTML($sTmpValue);
													$xpath = new DOMXPath($doc);
													$aImg=$xpath->evaluate('//img');
													$allImages = array();
													if($aImg->length>0) {
														foreach($aImg as $val)
														{
															$extractedImg = array();
															$uploadfilepath = explode("upload/",$val->getAttribute('src') );
															$extractedImg[name] = basename ( $uploadfilepath[1] );
															$uploaddirpath = explode($extractedImg[name], $uploadfilepath[1] );
															$extractedImg[path] = $uploaddirpath[0];
															array_push($allImages, $extractedImg);
														}
														$ChosenImage[name] = $allImages[0][name] ;
														$ChosenImage[path] = $allImages[0][path] ;
													}
                                                }

                                                if ($bDebug) {
                                                    echo '<pre>'.$sql.'</pre>';
                                                }
						
						if ( isset($ChosenImage) ) { // corr von chris8408
                                                // execute query and calculate image values
												$thisServerPath   = $cfgClient[$client]['upl']['path'].$ChosenImage[path].$ChosenImage[name];
												$image            = $cfgClient[$client]['upl']['htmlpath'].$ChosenImage[path].$ChosenImage[name];
												list ($width, $height, $type, $attr ) = getimagesize($thisServerPath);

												// print $k."elementwidth: ". $aSettings['k'.$k]['ElementWidth'] . "elementheight ".$aSettings['k'.$k]['ElementHeight'] ;

												if ($aSettings['k'.$k]['ElementWidth'] > 0 && $aSettings['k'.$k]['ElementHeight'] > 0 && file_exists($thisServerPath) &&
												($width > $aSettings['k'.$k]['ElementWidth'] || $height > $aSettings['k'.$k]['ElementHeight'])) {
												// Scale image
												$image = capiImgScale($thisServerPath, $aSettings['k'.$k]['ElementWidth'], $aSettings['k'.$k]['ElementHeight'], false, false, 10, true, 100, true);
												$image = str_replace($cfgClient[$client]['path']['htmlpath'], '', $image);

												// Get dimensions of the image
												list ($width, $height, $type, $attr ) = getimagesize($image);
												}
												$metatags = new cApiUploadMeta();
												$metatags->loadByUploadIdAndLanguageId( $db4->f('idupl'), $lang );
												$article[$k."_img"]       = $image;
												$article[$k."_img_src"]       = $cfgClient[$client]['upl']['htmlpath'].$db4->f('dirname').$db4->f('filename');
												$article[$k."_width"]          = $width;
												$article[$k."_height"]         = $height;
												$article[$k."_medianame"]      = $metatags->get("medianame");;
												$article[$k."_description"]    = $metatags->get("description");;
												$article[$k."_keywords"]       = $metatags->get("keywords");;
												$article[$k."_copyright"]      = $metatags->get("copyright");;
												$article[$k."_internal_description"] = $metatags->get("internal_description");;
							    
						}
                                                break;
                                            case ($aSettings['k'.$k]['ElementType'] == 'FixedDate'):
						$dom = new domDocument;
						$dom->loadXML($db3->f('value'));
						$xml = simplexml_import_dom($dom);
						$date_Ymd  = date('Y-m-d',  (int)$xml->timestamp);
						$oDate = new DateTimeReplaceLang( $date_Ymd );
						$article[$k."article_long"]   = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'long');
						$article[$k."article_short"]  = $oDate->format($aSettings['DateFormat'], $aSettings['DateLanguage'], 'short');
						$article[$k."_date"]   = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article[$k."article_long"] : $article[$k."article_short"];
						$date_interim1 = $oDate->format('Y-M-d', $aSettings['DateLanguage'], 'short');
						$date_interim = explode("-", $date_interim1);
						$article[$k."_date_day"]   = $date_interim[2];
						$article[$k."_date_month"]   = $date_interim[1];
						$article[$k."_date_year"]   = $date_interim[0];
						
                                                break;
                                            default:
						 
						//	quite likely, the text engine following is buggy if client's language setting is something other than utf8
						// 	needed to be changed as some symbols (+) where deleted if urldecode was applied to strings.
						$article[$k."_text_full"]         = text2adapt( $db3->f('value') , false , $aSettings['TextConversion'] );
						$article[$k."_text_strippedabit"] = str_replace('  ', ' ', preg_replace('/\r/s', '', preg_replace('/\n|\t/s', ' ', strip_tags($db3->f('value'), $aSettings['k'.$k]['ElementAllowedTags']))));
						$article[$k."_text_stripped"]     = text2adapt( $db3->f('value') , true , $aSettings['TextConversion'] );
                                                $article[$k."_text_cut"]          = cString::trimAfterWord($article[$k."_text_stripped"], $aSettings['k'.$k]['ElementWidth']).'...';
					
						if ( $aSettings['k'.$k]['DontStripTags'] ) {
							if ( strlen($aSettings['k'.$k]['ElementAllowedTags']) != 0 ) {
								$article[$k."_text"] = $article[$k."_text_strippedabit"];
							} else {
								$article[$k."_text"] = $article[$k."_text_full"];
							}
						} else {												
							if ( $aSettings['k'.$k]['ElementWidth'] > 0 && strlen($article[$k."_text_stripped"]) > $aSettings['k'.$k]['ElementWidth'] ) {
								$article[$k."_text"] = $article[$k."_text_cut"]; 
							} else {
								$article[$k."_text"] = $article[$k."_text_stripped"];
							}						
						}

						//$article[$k."_text"] = text2adapt( $article[$k."_text"] , false , $aSettings['TextConversion'] );

						// special customer request. extracts <h3>-tags from news that are used as headlines
						$anfang = strpos($article[$k."_text_full"] ,"<h3>")+4;
						$ende   = strpos($article[$k."_text_full"] ,"</h3>");
						$num    = ($ende - $anfang);
						$article[$k."_h3"] = substr($article[$k."_text_full"], $anfang, $num);

                                        } // end switch
                                    } // end if ($sTypeIdentifier == $aSettings['k'.$k]['Identifier'])
                                } // end for
				
                            } // end while
                        }
                        $i++;
		//add this article to our main array
		array_push($articleList, $article);			
                    }
                    elseif ($lRow == $lEndCount) {
                        break;
                    }
                    $lRow++;	
		
            } // end while

            // Clearing memory
            unset ($db2);
            unset ($db3);
            unset ($db4);
            unset ($sql);
            unset ($lStartIDArtLang);
            unset ($lStartCount);
            unset ($lEndCount);
            
            /**
             * Page browsing
             * Hint: Number of max available records: $lCount
             * Number of pages: ceil($lCount / $aSettings['ArticlePerPage'])
             * First page: page=1;
             * Last page:  page=ceil($lCount / $aSettings['ArticlePerPage']);
             */

	    	$sPagination = array();

                $iPages = ceil($lCount / $aSettings['ArticlePerPage']);

                if ($bDebug) {
                    echo '<p>$iPages = '.$iPages.' / $aSettings[\'currentPage\'] = '.$aSettings['CurrentPage'].'</p>';
                }
                
                // Example: Back and Next
                $sPaginationType01 = array();

		if ($iPages > 1 && $aSettings['CurrentPage'] > 1) {
		$aParams = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] - 1));
		$sPaginationType01[0][0] = cUri::getInstance()->buildRedirect($aParams);
		} else {
		$sPaginationType01[0][0] = '';
		}
		$sPaginationType01[0][1] = 'back';

		if ($iPages > 1 && $aSettings['CurrentPage'] < $iPages) {
		$aParams = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] + 1));
		$sPaginationType01[1][0]  = cUri::getInstance()->buildRedirect($aParams);
		} else {
		$sPaginationType01[1][0] = '';
		}
		$sPaginationType01[1][1] = 'next';
		
		if ( $aSettings['PaginationType'] == 1 )  array_push($sPagination, $sPaginationType01);

                // Example: 1 2 3 4 5 6 7 ...
                $sPaginationType02 = array();

		for ($i = 1; $i <= $iPages; $i++) {
			
			$aParams = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
			if ( $aSettings['CurrentPage'] != $i) {
				$sPaginationType02[] = array( cUri::getInstance()->buildRedirect($aParams), $i);
			} else {
				$sPaginationType02[] = array( '', $i);
			}
		}
		if ( $aSettings['PaginationType'] == 2 )  array_push($sPagination, $sPaginationType02);


                // Example: Seite 1 von 7
		$sPaginationType03[0][0] = '';
		$sPaginationType03[0][1] = mi18n("Page %s of %s", $aSettings['CurrentPage'], $iPages);
		if ( $aSettings['PaginationType'] == 3 )  array_push($sPagination, $sPaginationType03);

                // Example: << < 1 2 3 ... 5 6 7 ... 9 10 11 > >>
		$sPaginationType04 = array();
                    if ($aSettings['CurrentPage'] != 1) {
                        $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => 1);
                        $sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("«") );
                    } else {
                        $sPaginationType04[]   = array( '', mi18n("«") );
                    }

		    if (($aSettings['CurrentPage'] - 1) >= 1) {
                        $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] - 1));
			$sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("‹") );
                    } else {
			$sPaginationType04[]   = array( '', mi18n("‹") );
                    }
                    
                    for ($i = 1; $i <= $iPages; $i++) {
                        $aStartRange = array(1, 2, 3);
                        $aEndRange   = array($iPages, ($iPages-1), ($iPages-2));
                        if (in_array($i, $aStartRange) || in_array($i, $aEndRange)) {
                            $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
				if ($aSettings['CurrentPage'] != $i) {
				$sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), $i );
				} else {
				$sPaginationType04[]   = array( 'active', $i ); // I'm on the current page, no link
					}
                        } elseif ($aSettings['CurrentPage'] == ($i+1)) {
                            // Front
                            $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
                            $sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), $i );
                        } elseif ($aSettings['CurrentPage'] == ($i-1)) {
                            // Back
                            $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
                            $sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), $i );
                        } elseif ($aSettings['CurrentPage'] == ($i-2) ||
                                  $aSettings['CurrentPage'] == ($i+2) ||
                                    ($aSettings['CurrentPage'] == ($i-3) && (($i-3)==1)) ||
                                    ($aSettings['CurrentPage'] == ($i+3) && (($i+3)==$iPages))) {
                            $sPaginationType04[]   = array( '', ' ...' );
                        } elseif  ($aSettings['CurrentPage'] == ($i)) {
			    $sPaginationType04[]   = array( 'active', $i ); // I'm on the current page, no link
                        } else  {
                            $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
                            $sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), $i );
			}
                    }
                    
                    if (($aSettings['CurrentPage'] + 1) <= $iPages) {
                        $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] + 1));
			$sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("›") );
                    } else {
                        $sPaginationType04[]   = array( '', mi18n("›") );
                    }
                    
                    if ($aSettings['CurrentPage'] != $iPages) {
                        $aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $iPages);
			$sPaginationType04[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("»") );
                    } else {
                        $sPaginationType04[]   = array( '', mi18n("»") );
                    }
		if ( $aSettings['PaginationType'] == 4 )  array_push($sPagination, $sPaginationType04);



                // Example: << < 1 2 3 4 5 6 7 8 ... > >>
                // This version is newly programmed and will produce errors for a start
		$sPaginationType05 = array();
		if ($aSettings['CurrentPage'] != 1) {
			$aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => 1);
			$sPaginationType05[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("«") );
			} else {
			$sPaginationType05[]   = array( '', mi18n("«") );
		}

		if (($aSettings['CurrentPage'] - 1) >= 1) {
			$aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] - 1));
			$sPaginationType05[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("‹") );
			} else {
			$sPaginationType05[]   = array( '', mi18n("›") );
		}
		if ($iPages > $aSettings['PaginationType05MaxPages'] && ($aSettings['CurrentPage'] + 1) > $aSettings['PaginationType05MaxPages']) $sPaginationType05[] = array( '', ' ...' );

		$start = ( $aSettings['CurrentPage'] > $aSettings['PaginationType05MaxPages']) ? $aSettings['CurrentPage']-$aSettings['PaginationType05MaxPages'] : 1;
		$end = ( $start != 1 ) ? $aSettings['CurrentPage'] : $iPages ;

		for($i = $start; $i <= $end; $i ++) {
			if ($i == $aSettings['CurrentPage'] ) {
				$sPaginationType05[]   = array( '', $i );
			} else {
				$aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $i);
				$sPaginationType05[]   = array( cUri::getInstance()->buildRedirect($aParams), $i );
			}
		}

		if ($iPages > $aSettings['PaginationType05MaxPages'] && ($aSettings['CurrentPage'] + 1) <= $aSettings['PaginationType05MaxPages']) $sPaginationType05[] = array( '', '... ' );
		if (($aSettings['CurrentPage'] + 1) <= $iPages) {
			$aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => ($aSettings['CurrentPage'] + 1));
			$sPaginationType05[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("‹") );
		} else {
			$sPaginationType05[]   = array( '', mi18n("‹") );
		}

		if ($aSettings['CurrentPage'] != $iPages) {
			$aParams            = array('lang' => $lang, 'idcat' => $idcat, 'idart' => $idart, 'page' => $iPages);
			$sPaginationType05[]   = array( cUri::getInstance()->buildRedirect($aParams), mi18n("»") );
		} else {
			$sPaginationType05[]   = array( '', mi18n("»") );
		}		    
		if ( $aSettings['PaginationType'] == 5 )  array_push($sPagination, $sPaginationType05);

		// Pagination calculation finished

	    // end of IF articles found; if none are found, deal with it in the html-template
	    } 


	if ( $bDebug ) { print "<pre>"; print_r($articleList); print "</pre>"; }

	// nun noch ein paar Hauptwerte übergeordneter Art

	// Generate article list headline
	$listObj->artlistHeadline = text2adapt( $aSettings['ArtListHeadline'] , false , $aSettings['TextConversion'] );	

	// Generate overview link. check on string being empty is mandatory, or cUri will crash the script
	if ( $aSettings['OverviewLink'] != '' ) {
		$aParams = (($iCount = substr_count($aSettings['OverviewLink'],'cat_')) > 0) ? array('lang' => $lang, 'idcat' => str_replace('cat_', '', $aSettings['OverviewLink'])) : '';
		$aParams = (($iCount = substr_count($aSettings['OverviewLink'],'art_')) > 0) ? array('lang' => $lang, 'idcatart' => str_replace('art_', '', $aSettings['OverviewLink'])) : $aParams; // corr von rethus replaced idart with idcatart

		$listObj->overviewLink = cUri::getInstance()->buildRedirect($aParams);
	}
		
	// Generate a) default article text or b) free default text in case the article list will be empty
	// regarding the output priority: chosen article text ranks higher than free text
	$listObj->nolistFreetext = $aSettings['DefaultTextFree']; 
	if ( $aSettings['DefaultTextArticle'] != '' ) {
		$oCatArt = new cApiCategoryArticle( $aSettings['DefaultTextArticle']);
		$thisidart = $oCatArt->getField("idart");
		$oArt = new cApiArticleLanguage( $thisidart, $lang);
		$oArt->loadByArticleAndLanguageId( $thisidart, $lang, true);
		$listObj->nolistArticletext = $oArt->getContent('html', 1); 
	}
	$listObj->nolistText = ( $listObj->nolistArticletext != '' ) ? $listObj->nolistArticletext : $listObj->nolistFreetext ;

	$listObj->more1         = mi18n("more_linktext1");
	$listObj->more2         = mi18n("more_linktext2");
	$listObj->more3         = mi18n("more_linktext3");
	$listObj->dateLabel1    = mi18n("date_label1");
	$listObj->dateLabel2    = mi18n("date_label2");
	$listObj->dateLabel3    = mi18n("date_label3");
	$listObj->overviewText1 = mi18n("overview_text1");
	$listObj->overviewText2 = mi18n("overview_text2");
	$listObj->overviewText3 = mi18n("overview_text3");
	$listObj->authorLabel1  = mi18n("author_label1");
	$listObj->authorLabel2  = mi18n("author_label2");
	$listObj->authorLabel3  = mi18n("author_label3");

	// create template
	$tpl->assign('actidcat', $idcat); // some customers require idcat too
	$tpl->assign('alrId', "articleList"); // sets main container ID
	$tpl->assign('realidart', $idart); 
	$tpl->assign('listObj', $listObj); // an object
	$tpl->assign('allPaginations',  $sPagination); // an array to walk through in template
	$tpl->assign('paginationType1', $sPaginationType01); // an array to walk through in template
	$tpl->assign('paginationType2', $sPaginationType02); // an array to walk through in template
	$tpl->assign('paginationType3', $sPaginationType03); // an array to walk through in template
	$tpl->assign('paginationType4', $sPaginationType04); // an array to walk through in template
	$tpl->assign('paginationType5', $sPaginationType05); // an array to walk through in template

        $tpl->assign('articleList', $articleList ); // an array to walk through in template

	if ( file_exists($tplpath.$aSettings['Template']) ) {
		$tpl->display($aSettings['Template']);
	} else {
		print mi18n("no_template_found");
	}
    }
    // Clearing memory
    unset ($aSettings);
    unset ($i);
    unset ($lCount);
    unset ($bDebug);
    unset ($db5);
} elseif ($bDebug) {
    echo '<strong>ShowModuleSetting: '.$aSettings['ShowModul'].'</strong>';
} else {
    // do nothing
}

?>