%s. Element=%s. Element
Add Date=Erstellungsdatum
Additional configuration=Weitere Einstellungen

Article ID=Artikel-ID
Dec=Dez.
December=Dezember
Fri=Fr
Headline=Headline
Identify Elements=Elemente zuweisen

Last Modified Date=&Auml;nderungsdatum
Link to overview=Link zur Übersichtsseite

Main configuration=Grundeinstellung

Page %s of %s=Seite %s von %s
Page Title (Backend)=Seitentitel (Backend)
Page Title (Frontend)=Seitentitel (Frontend)
Published Date=Ver&ouml;ffentlichungsdatum
Random=Zuf&auml;llig
Save changes=Änderungen speichern

Sort No.=Sort-Nr.
Summary (Backend)=Zusammenfassung (Backend)
Text=Text
Type=Inhaltstyp und Quellcontainer festlegen


allowed_tags=Zugelassene Tags
ausschließlich Startartikel anzeigen=ausschließlich Startartikel anzeigen
author_label1=erstellt von
author_label2=Autor: 
author_label3=ein Beitrag von
date_label1=Artikel vom
date_label2=veröffentlicht am
date_label3=Nachricht vom
height_max=max. Höhe
info_addvalues_translation_section=Für den Text zum Link sowie andere Beschreibungstexte (Label zum Wert "Autor", "weiter"-Button-Texte) stehen je Typ 3 Variablen in der Übersetzung bereit.
keep_menu_open=Einstellungsfenster nicht einklappen
keep_tag_question=Tags im Text belassen? Text wird dann nicht gekürzt!
label_adddir=Zusätzliche Kategorien:
label_addelements=Zusätzliche Elemente:
label_amountofarticles=Anzahl Artikel:
label_artheadline=Headline-Element:
label_artlistheadline=Überschrift für Artikelliste:
label_artperpage=Artikel pro Seite
label_behaviour=Darstellung der grauen Boxen unten:
label_category_main=Primäre Kategorie:
label_date=Datum:
label_nolistarticle=Text, wenn kein Artikel vorhanden:
label_nolistfreetext=Freier Text:
label_online=Modul aktivieren:
label_overviewlink=Link zur Übersichtsseite:
label_pagetitle=Seitentitel kürzen auf:
label_pagination=Pagination: 
label_samplearticle=Beispielartikel:
label_sort=Sortiert nach:
label_startarticle=Startartikel:
label_sub_textchanges=ja (in functions.alr.php zu programmieren)
label_summary=Zusammenfassung kürzen auf:
label_template=Vorlage zur Darstellung auswählen:
label_textchanges=Eigene Textänderungsroutine anwenden:
label_thisarticle=Aktuell gewählter Artikel:
length=Zeichen
length_max=max. Länge
maximum_pages_for_style5=max. Länge Zahlenblock für Typ 05:
mit einschließen=mit einschließen
more_linktext1=mehr
more_linktext2=weiterlesen
more_linktext3=zum Artikel
nicht anzeigen. Aktuelle idart: =nicht anzeigen. Aktuelle idart:
nicht mit einschließen=nicht mit einschließen
no_template_found=Bitte ein Template zuweisen in der Artikelkonfiguration!
none=(kein)
overview_text1=Alle Artikel in der Übersicht
overview_text2=Alle News auf einen Blick
overview_text3=Weitere News
please_choose=Bitte wählen
use_pagetitle_as_headline=als Headline verwenden (überschreibt Artikel und Zusammenfassung)
use_summary_as_headline=als Headline verwenden (überschreibt Artikelwahl)
value_ascending=aufsteigend
value_below=Alle unterhalb primärer
value_descending=absteigend
value_none=Keine
value_select=Ausgewählte
width_max=max. Breite




«=
»=
‹=
›=




































