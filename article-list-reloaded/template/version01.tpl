﻿{* Mastervorlage für die Article List Reloaded    *}
{*                                                *}
{* LISTE ALLER VARIABLEN AM ENDE DIESER DATEI!!!! *}
{*                                                *}
{* Das Modul erzeugt und übergibt weit mehr Werte, als in der Vorlage konfiguriert werden (Liste s.u.). *}
{* In der Vorlagenkonfiguration werden aus der Vielzahl der erzeugten Werte lediglich eine Auswahl in "default"-values gespeichert, so dass sich diese hier variabel verhalten, wenn man die Vorlage ändert. *}
{* Alle anderen Werte stehen dennoch zur Verfügung und können bei händischer Anpassung hier eingesetzt werden. *}
{* So ist es denkbar, sowohl z.B. einen gekürzten Seitentitel als Headline zu verwenden und den ungekürzten Seitentitel als Bildbeschreibung usw. *}
{* Zudem stehen für einige Fälle standardmäßig 3 zusätzliche Variablen zur Auswahl, falls z.B. verschiedene Versionen von "weiter"-Links zum Einsatz kommen müssen. *}
{* Auch jede Pagination steht zusätzlich zur jeweils ausgewählten in je eigenen Variablen zur Verfügung. *}
{* Diese Seite zeigt im Beispielcode nur EINE mögliche Darstellung, und die Elementnummern müssen jeweils neu zugeordnet werden. *}
{* NEU: Ein Artikelelement behält seine laufende Nummer aus der Vorlagenkonfiguration, also: "2. Element" =  $article.2_img  *}


<div id="{$alrId}">

	{if !empty($articleList) }
	
	{if $listObj->artlistHeadline != '' }
			<h1>{$listObj->artlistHeadline}</h1>
		{/if}
	
		
		<div class="newsPagination">
			
			{if !empty($allPaginations) }
				{foreach from=$allPaginations item=paginationtype}
				
					<div class="pagination paginationtype clearfix">
					{foreach from=$paginationtype item=page}		
						{if $page.0 == '' }
						<a class="active">{$page.1}</a>
						{else}
						<a href="{$page.0}" class="pageCounter">{$page.1}</a>&nbsp;
						{/if}
					{/foreach}
					</div>
				
				{/foreach}
			{/if}
			
		</div>
		
		
		{foreach from=$articleList item=article}
	
			<div class="newsArticle clearfix">
			
			{if $article.date != '' }
				<p class="artDate">{$listObj->dateLabel1} {$article.date}<p>
			{/if}
			{if $article.author != '' }
				<p class="artAuthor">{$listObj->authorLabel1} {$article.author}</p>
			{/if}
			{if $article.headline != '' }
				<h2>{$article.headline}</h2>
			{/if}	

				<div class="artImage">
					<a href="{$article.2_imgSrc}"><img src="{$article.2_img}" width="{$article.2_width}" height="{$article.2_height}" alt="{$article.2_medianame}" title="{$article.2_medianame}"></a>
				</div>
				<p class="artText">{$article.1_text}<br />{$article.3_text}</p>
			
			{if $article.link != '' }
				<p class="linkMore"><a href="{$article.link}">{$listObj->more1}</a></p>
			{/if}					
	
			</div>

		{/foreach}

	{if $listObj->overviewLink != '' }
		<p class="linkOverview"><a href="{$listObj->overviewLink}">{$listObj->overviewText1}</a></p>
	{/if}	
	
	
	{else} {* if articleList == empty *}
	
	<p>{$listObj->nolistText}</p>
	
	{/if}

</div>





{*

Auf die Liste bezogene Variablen als Objekt:
$listObj->artlistHeadline  // in der Konfiguration eingegebene Überschrift über die ganze Liste
$listObj->authorLabel1 // 1-3 enthalten über die Übersetzung konfigurierbare Texte als Umfeld zu $article.author ("veröffentlicht von...")
$listObj->authorLabel2
$listObj->authorLabel3
$listObj->dateLabel1 // 1-3 enthalten über die Übersetzung konfigurierbare Texte als Umfeld zu $article.date ("veröffentlicht am...")
$listObj->dateLabel2
$listObj->dateLabel3
$listObj->more1  // 1-3 enthalten über die Übersetzung konfigurierbare Links für "weiter"-Buttons
$listObj->more2
$listObj->more3
$listObj->overviewLink // Link zu einer weiterführenden Übersichtsseite, z.B. mit ALLEN Teasern statt nur einer Auswahlteaserliste
$listObj->overviewText1 // 1-3 enthalten über die Übersetzung konfigurierbare Linktexte für den Link zur weiterführenden Seite (Übersicht)
$listObj->overviewText2
$listObj->overviewText3
$listObj->nolistArticletext // enthält ggf. den Text aus dem CMS_TEXT[1] des in der Konfiguration genannten Artikels
$listObj->nolistFreetext // enthält den in der Konfiguration angegebenen freien Text
$listObj->nolistText // Dynamische Variable, enthält je nach Einstellung entweder $listObj->nolistFreetext oder $listObj->nolistArticletext 

Arrays für die Pagination:
allPaginations  // enthält alle 5 u.g. Paginationen in einem mehrdimensionalen Array. Abhängig vom Paginationsdropdown wird aber nur die gewünschte ausgegeben.
paginationType1 // enthält Typ 1 als Array. $page.0 ist der Link, $page.1 ist der Linktext
paginationType2
paginationType3
paginationType4
paginationType5

Dynamisch artikelbezogene Variablen:
// diese passen ihren Inhalt an, je nach Einstellungen in der Konfiguration der Vorlage
// daher sind diese bevorzugt zu verwenden, da die Änderungen in der Konfiguration sich dann auch sichtbar auswirken:
article.headline // enthält entweder einen Containertext, die Zusammenfassung oder den Seitentitel, ggf. sogar gekürzt, je nach Einstellung
article.headline_stripped // enthält entweder einen Containertext, die Zusammenfassung oder den Seitentitel, ohne Tags
$article.date // enthält das in der Konfiguration ausgewählte Datum (created, published oder lastmodified) in der gewünschten Formatierung
// folgende Datumsvariablen passen sich an einen TEIL der Konfiguration an, nämlich das Datumsformat
$article.lastmodified // Datum, wann Artikel zuletzt geändert wurde, enthält entweder Variante long oder short
$article.created // wie lastmodified, nur bezogen auf das Erstellungsdatum des Artikels
$article.published // wie lastmodified, nur bezogen auf das Veröffentlichungsdatum
// folgende Textvariablen passen sich an, falls Kürzungen in der Konfiguration gewünscht sind oder nicht:
$article.pagetitle // enthält entweder die lange oder gekürzte Form des Seitentitels, je nach Einstellung
$$article.summary // Zusammenfassung (erstellbar in den Artikeleigenschaften), enthält entweder die lange oder gekürzte Form, je nach Einstellung


Weitgehend statische Variablen:
// von den Einstellungen in der Konfiguration nicht beeinflusst
$actidcat // entspricht der idcat des Listenartikels, der dieses Template gerade zusammenbastelt 
$realidart // entspricht der idart des Listenartikels, der dieses Template gerade zusammenbastelt
$article.number  // fortlaufende Nummer ab 1 pro ausgegebenem Artikel
$article.category // Echtname der Kategorie, in zu der der Artikel gehört
$article.categoryid // idcat der Kategorie, zu der der Artikel gehört
$article.articleid // idart des Artikels
$article.link // Link zum ARTIKEL, aus dem sich der Teaser speist
$article.linkcat // Link zur KATEGORIE des Artikels (für "...more")
$article.lastmodified_long // Datum im Langformat, z.B. "13. Dezember 2012"
$article.lastmodified_short // Datum im Kurzformat, z.B. "13. Dez. 2012"
$article.created_long
$article.created_short
$article.published_long
$article.published_short
$article.author //  Echtname des Autors
$article.headline_full // Ungekürzte Version des gewählten Containers in der Konfiguration
$article.headline_trimmed // Gekürzte Version, falls gewünscht
$article.pagetitle_full // Ungekürzter Seitentitel
$article.pagetitle_trimmed // Gekürzter Seitentitel, falls in der Konfiguration gewünscht
$article.summary_full // Ungekürzte Version der Zusammenfassung (erfassbar in den Artikeleigenschaften)
$article.summary_trimmed // Gekürzte Version der Zusammenfassung, falls gewünscht

Einzelne Artikelelemente im Stil, wobei die Nummer jeweils zu ersetzen ist:
// Wenn Text, dann
$article.1_text  // Containertext in der Variante, wie in der Konfiguration gewünscht
$article.1_text_full // Ungekürzte Version
$article.1_text_stripped // Version ohne jegliche Tags
$article.1_text_strippedabit // Version mit den verbliebenen gewünschten Tags
$article.1_text_cut // gekürzter Text ohne jegliche Tags

// Wenn Bild, dann
$article.2_img // Pfad zur Bilddatei
$article.2_img_src // Pfad zur Originaldatei, ggf. nutzbar für ein Popup/Lightbox innerhalb der Artikelliste
$article.2_width // Breite des (ggf. neu berechneten) Bildes
$article.2_height // Höhe des (ggf. neu berechneten) Bildes
$article.2_medianame // Metatag, einstellbar im Uploadbereich bei den Bildeigenschaften
$article.2_description // Metatag
$article.2_keywords // Metatag
$article.2_copyrights // Metatag
$article.2_internal_description // Metatag

*}