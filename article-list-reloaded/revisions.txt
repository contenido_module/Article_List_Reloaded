21.09.2013 (corr01)
BUG: Ist das Modul 2x eingeklemmt, fangen die Auswahllisten für zusätzliche Kategorien unheilvoll zu interagieren an. (Dank an benja)

28.10.2013 (corr02)
BUG: $aSettings['DateLanguage'] wurde bisher nicht gesetzt, zieht seine Einstellung jetzt aus Administration -> Sprache -> Sprache für Datum / Zeit. Allerdings erfordert dies Erweiterungen für gewisse Sprachen im Modulordner /php/class.datetimereplacelang.php. 
Wer händisch anpassen will, ergänzt bitte im Moduloutput recht weit oben unterhalb von

    $aSettings = array();

folgende Zeilen:
    $oLang = new cApiLanguage();
    $oLang->loadByPrimaryKey($lang);
    $aSettings['DateLanguage'] = $oLang->getProperty("dateformat", "locale");


19.12.2013 (corr03)
BUG: Kam ein Bildername in unterschiedlichen Uploadordnern doppelt vor, so produzierte "Bild aus Text" gelegentlich Fehler und zog das falsche Bild. (Dank an Forenuser "Faar" für Code und Ideen).
BUG: Das Datumsformat wurde ggf. falsch ausgeschossen bei CMS_DATE. (Dank an Forenuser "Spl4sh3r" für Codekorrekturen).

17.07.2014 (corr04)
BUG: die im Forum genannten Bugs seit Mitte Dezember 2013 sind alle behoben (Dank an rethus, DoroM und chris8408).
UPDATE: US-Datumsstring ist nachgepflegt im Input (Vorschlag von rethus)

14.01.2015 (corr05)
Update: $article['articleid'] im Moduloutput als Variable ergänzt.
Update: $tpl->assign('realidart', $idart); ergänzt. 
Update: Hinweise in version01.tpl angepasst, dass diese beiden Variablen jetzt vorliegen
Update: Textengine-Code angepasst und urldecode-Befehle gelöscht, da dies Sonderzeichen verschluckt (+). Danke an benja für den Hinweis. Textengine KANN jetzt aber buggy sein, wenn Mandant nicht utf8 ist.

14.01.2015 (corr06)
Update: Vielen Dank an den User "dermicha". Seine Erweiterungen um Tagging-Code sind jetzt hier eingebaut und leicht angepasst, aber nicht getestet, da ich kein Projekt mit Tagging/Content Allocation habe. Sollte diese Version Probleme bereiten, dann auf die Tagging-freie corr05 umsteigen, die zur Not immer noch als eigener Zweig dienen kann, falls die Taggingversion zu buggy sein sollte. Bitte beachten, dass in /php eine neue Datei hinzugekommen ist. Modulinput und -output sind geändert, die anderen Dateien sind unverändert. Ggf. Sicherungen dieser Dateien anlegen.
Da diese Version sich schnell als buggy erwies, wurde sie zugunsten von corr07 gelöscht.

26.01.2015 (corr07)
Bug: function stand noch im Output. Bei mehrfachem Einsatz der ALR pro Template führte dies zu unerlaubtem Mehrfachaufruf und Absturz. Function ausgelagert.
Beautifier: functions.input.helper_contentallocation.php Code verschönert: num_rows -> numRows usw.

10.02.2015
Der Content-Type DATE wird zusätzlich noch in Einzelvariablen für Tag, Monat, Jahr zerlegt für alternative Kalenderanzeigen. Dies gilt derzeit noch nicht für Datumsangaben, die aus dem System selbst kommen.
class.datetimereplacelang.php: neue Sprachen hinzugefügt (Spanisch, Katalanisch)
WICHTIG: Für Datumsfunktionen sollte in Administration/Sprachen in der jeweiligen Sprache das Feld "Sprache für Datum/Zeit" korrekt gesetzt sein: de_DE, pt_PT, es_ES, ca_CA. Per Default werden englische Angaben erzeugt.

16.03.2015
Change: JS-Funktion fncAddMultiSelJS() aus functions.input.helper.gw.php umgelagert in js.input.alr.php, wo sie eleganter eingebunden wird. Modul-Input hierfür leicht angepasst.
Change: Function text2utf8 umbenannt in text2adapt und komplett umgeschrieben. Es werden weiterhin mehrere Parameter übergeben. Der Aufruf der Function bearbeitet einen String. Sicherheitshalber wird die Function auf möglichst jeden Text angewendet. Mit dem zweiten Parameter kann wie bisher strip_tags angewendet werden, der dritte Parameter ist "true", wenn im Inputbereich "Eigene Textänderungsroutine verwenden" angeklickt wurde. Dann lässt sich die Function durch den Programmierer so anpassen, dass weitere grundlegene Änderungen an den Strings vorgenommen werden, sofern dies je nach Codierung nötig ist. Für diese Anpassungen wurden die functions.alr.php sowie die Input- und Output-Datei des Modulcodes geändert. Die zuletzt falsch eingesetzten Aufrufe urldecode und html_entity_decode werden jetzt standardmäßig nicht mehr eingesetzt und sind für utf-8-Mandanten auch unnötig.

06.05.2015 (corr08)
BUG für Tagging: if-Schleife war falsch gesetzt, siehe "dermicha" http://forum.contenido.org/viewtopic.php?f=89&t=34196&start=75

31.05.2015
Bug im Input: entdeckt von benja, s. Forum: die Konfiguration "Ausgewählte Verzeichnisse" schlug sich auf die folgenden mehrfach eingebundenen ALRs durch. $cnumber musste zudem durch $cCurrentContainer ersetzt werden.

01.10.2015 (corr09)
Bug im Output: lastmodified wurde als Datum nicht korrekt bearbeitet, siehe http://forum.contenido.org/viewtopic.php?f=89&t=36583
Alt: $article['lastmodified'] = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article['lastmodified'] : $article['lastmodified'];
neu: $article['lastmodified'] = ($aSettings['DateFormat'] == 'F j, Y' || $aSettings['DateFormat'] == 'j F Y') ? $article['lastmodified_long'] : $article['lastmodified_short'];

26.10.2015
Bug im Input: deprecated scanDirectory für Smarty-Templates wurde ersetzt, Dank an rethus. Überflüssige Zeile im Input gelöscht.
Bug im Output, ca. Zeile 402: Die sql-Abfrage für elements wurde 1x zuviel durchlaufen. Ein Gleichheitsoperator musste raus:
  ORIGINAL: for ($k = 1; $k <= $aSettings['Elements']; $k++) {
  NEU     : for ($k = 1; $k < $aSettings['Elements']; $k++) {

18.05.2016
Zeile 402 nach Userrückmeldungen doch wieder auf Originalversion zurückgebaut, da beständig ein Element fehlte...

19.05.206
Bug im Output: deprecated capiStrTrimAfterWord durch cString::trimAfterWord ersetzt. Danke an dermicha.

--- not yet documented

(nothing)
