Article List Reloded (ALR) 
==========================

**Type:** 		Contenido Module  
**Name:** 		article_list_reloaded  
**Forum:**      http://forum.contenido.org/viewtopic.php?f=99&t=34196

**Contenido-Version:** >= 4.9.8
_or copy cString from 4.9.8 to your 4.9.x Installation as Workarround_

[CHANGELOG](https://gitlab.com/xstable/Article_List_Reloaded/blob/master/CHANGELOG.md)