# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.1.0] - 2016-11-30
### Added
 - Project ported to Gitlab. Init with 0.1.0 = corr13 from 2016-08-12 (see http://forum.contenido.org/viewtopic.php?f=99&t=34196)

## Previous Versions
 - Previous Changelog in "revisions.txt" and in german Language. Since Version 0.1 Changelog is here and in English.**